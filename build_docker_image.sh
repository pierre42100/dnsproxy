#!/bin/bash
cargo build --release

TEMP_DIR=$(mktemp -d)
cp target/release/dns_proxy "$TEMP_DIR"

docker build -f Dockerfile "$TEMP_DIR" -t pierre42100/dns_proxy

rm -r $TEMP_DIR
