FROM debian:bookworm-slim

RUN apt-get update \
  && apt-get install -y ca-certificates \
  && rm -rf /var/lib/apt/lists/*


ENV LISTEN_ADDR="0.0.0.0:5300"
ENV UPSTREAM_ADDR="1.1.1.1:53"
ENV SRC_IP="216.58.213.174"
ENV DST_IP="1.1.1.3"

COPY dns_proxy /usr/local/bin/dns_proxy

ENTRYPOINT ["/usr/local/bin/dns_proxy"]
