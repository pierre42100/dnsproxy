use std::borrow::Borrow;
use std::collections::{HashMap, HashSet};
use std::io;
use std::io::ErrorKind;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;

use clap::Parser;
use dns_proxy::args::{Args, IPTransformation, RecordsMap};
use hickory_resolver::config::{ResolverConfig, ResolverOpts};
use hickory_resolver::name_server::TokioConnectionProvider;
use hickory_resolver::proto::rr::{DNSClass, RData};
use hickory_resolver::TokioResolver;
use hickory_server::authority::{
    AnyRecords, Authority, Catalog, LookupControlFlow, LookupError, LookupOptions, LookupRecords,
    MessageRequest, UpdateResult, ZoneType,
};
use hickory_server::proto::op::ResponseCode;
use hickory_server::proto::rr::rdata::NULL;
use hickory_server::proto::rr::{rdata, LowerName, Name, Record, RecordSet, RecordType};
use hickory_server::server::RequestInfo;
use hickory_server::ServerFuture;
use k8s_openapi::api::networking::v1::Ingress;
use kube::api::ListParams;
use tokio::net::{TcpListener, UdpSocket};

struct Config {
    resolver: TokioResolver,
    ip_transform: HashMap<IpAddr, Vec<IPTransformation>>,
    local_entries: HashMap<(RecordType, Name), Record>,
}

impl Config {
    fn apply_transform(&self, entry: IpAddr, client: Option<IpAddr>) -> IpAddr {
        if let Some(entries) = self.ip_transform.get(&entry) {
            for t in entries {
                match client {
                    Some(client) => {
                        if t.net.contains(&client) && !t.exclude.iter().any(|n| n.contains(&client))
                        {
                            return t.dst;
                        }
                    }
                    None => {
                        if t.net.netmask().is_unspecified() {
                            return t.dst;
                        }
                    }
                }
            }
        }

        entry
    }

    fn apply_transform_v4(&self, ipv4_entry: Ipv4Addr, client: Option<IpAddr>) -> Ipv4Addr {
        match self.apply_transform(ipv4_entry.into(), client) {
            IpAddr::V4(v) => v,
            _ => panic!(),
        }
    }

    fn apply_transform_v6(&self, ipv6_entry: Ipv6Addr, client: Option<IpAddr>) -> Ipv6Addr {
        match self.apply_transform(ipv6_entry.into(), client) {
            IpAddr::V6(v) => v,
            _ => panic!(),
        }
    }
}

fn transform_rdata(config: Arc<Config>, rec: Option<&RData>, client: Option<IpAddr>) -> RData {
    match rec {
        None => RData::NULL(NULL::new()),
        Some(RData::A(ip)) => RData::A(rdata::A(config.apply_transform_v4(ip.0, client))),
        Some(RData::AAAA(ip)) => RData::AAAA(rdata::AAAA(config.apply_transform_v6(ip.0, client))),
        Some(e) => e.clone(),
    }
}
async fn dispatch_lookup(
    name: &Name,
    rtype: RecordType,
    config: &Arc<Config>,
) -> Result<(Vec<Record>, bool), LookupError> {
    match config
        .local_entries
        .get(&(rtype, name.clone()))
        .or_else(|| config.local_entries.get(&(RecordType::CNAME, name.clone())))
    {
        // We have got a local response
        Some(record) => Ok((vec![record.clone()], true)),

        // We need to forward the request
        None => {
            // First, send request to upstream DNS server
            let response = config.resolver.lookup(name.clone(), rtype).await;

            // Check for error
            match response {
                Ok(r) => Ok((r.records().to_vec(), false)),
                Err(e) if e.is_no_records_found() => Ok((vec![], false)),
                Err(e) => {
                    log::error!("Failed to forward request! {e}");
                    Err(LookupError::Io(io::Error::new(ErrorKind::Other, e)))
                }
            }
        }
    }
}

/// Extract target name of CNAME record
fn extract_cname_target(r: &Record) -> Option<Name> {
    match r.data() {
        RData::CNAME(n) => Some(n.0.clone()),
        _ => None,
    }
}

async fn perform_lookup(
    name: Name,
    rtype: RecordType,
    config: Arc<Config>,
    client_ip: Option<IpAddr>,
) -> Result<<MyAuthority as Authority>::Lookup, LookupError> {
    let (mut records, is_local) = dispatch_lookup(&name, rtype, &config).await?;

    // Check if we got a cached CNAME record. If it is the case, try to recursively resolve the
    // target
    if is_local
        && records
            .last()
            .map(|r| r.record_type().is_cname())
            .unwrap_or_default()
    {
        let mut visited = HashSet::new();
        visited.insert(name.clone());

        while let Some(next) = extract_cname_target(&records[records.len() - 1]) {
            if visited.contains(&next) {
                log::warn!("Loop detected while resolving cname for {} !", name);
                break;
            }

            let (mut new_res, new_local) = dispatch_lookup(&next, rtype, &config).await?;
            records.append(&mut new_res);
            visited.insert(next);

            if !new_local {
                break;
            }
        }
    }

    if records.is_empty() {
        return Ok(LookupRecords::Empty);
    }

    let mut records_set: Vec<RecordSet> = Vec::new();
    for rec in records.iter() {
        match records_set.iter_mut().find(|r| {
            r.name() == rec.name()
                && r.record_type() == rec.record_type()
                && r.dns_class() == rec.dns_class()
        }) {
            None => {
                let mut rs = RecordSet::new(rec.name().clone(), rec.record_type(), 2);
                rs.add_rdata(transform_rdata(config.clone(), Some(rec.data()), client_ip));
                records_set.push(rs);
            }
            Some(rs) => {
                rs.add_rdata(transform_rdata(config.clone(), Some(rec.data()), client_ip));
            }
        }
    }
    let mut records_set = records_set.into_iter().map(Arc::new).collect::<Vec<_>>();
    records_set.reverse();

    // Then build response
    let record = if rtype == RecordType::ANY || rtype == RecordType::AXFR {
        LookupRecords::AnyRecords(AnyRecords::new(
            LookupOptions::default(),
            records_set,
            rtype,
            LowerName::new(&name),
        ))
    } else if records_set.len() == 1 {
        LookupRecords::Records {
            lookup_options: LookupOptions::default(),
            records: records_set.remove(0),
        }
    } else {
        LookupRecords::ManyRecords(LookupOptions::default(), records_set)
    };

    Ok(record)
}

struct MyAuthority {
    proxy_origin: LowerName,
    config: Arc<Config>,
}

impl MyAuthority {
    pub fn new(config: Arc<Config>) -> Self {
        Self {
            proxy_origin: LowerName::new(&Name::root()),
            config,
        }
    }
}

#[async_trait::async_trait]
impl Authority for MyAuthority {
    type Lookup = LookupRecords;

    fn zone_type(&self) -> ZoneType {
        ZoneType::Forward
    }

    fn is_axfr_allowed(&self) -> bool {
        false
    }

    async fn update(&self, _update: &MessageRequest) -> UpdateResult<bool> {
        Err(ResponseCode::NotImp)
    }

    fn origin(&self) -> &LowerName {
        &self.proxy_origin
    }

    async fn lookup(
        &self,
        name: &LowerName,
        rtype: RecordType,
        _options: LookupOptions,
    ) -> LookupControlFlow<LookupRecords> {
        log::info!("forwarding lookup: {} {}", name, rtype);
        let name: &Name = name.borrow();

        LookupControlFlow::Break(
            perform_lookup(name.clone(), rtype, self.config.clone(), None).await,
        )
    }

    async fn search(
        &self,
        request_info: RequestInfo<'_>,
        _options: LookupOptions,
    ) -> LookupControlFlow<LookupRecords> {
        let name: &Name = request_info.query.name().borrow();

        LookupControlFlow::Break(
            perform_lookup(
                name.clone(),
                request_info.query.query_type(),
                self.config.clone(),
                Some(request_info.src.ip()),
            )
            .await,
        )
    }

    async fn get_nsec_records(
        &self,
        _name: &LowerName,
        _lookup_options: LookupOptions,
    ) -> LookupControlFlow<LookupRecords> {
        LookupControlFlow::Break(Err(LookupError::from(io::Error::new(
            io::ErrorKind::Other,
            "Getting NSEC records is unimplemented for the forwarder",
        ))))
    }
}

fn parse_ingress(ingress: &Ingress) -> Option<(Vec<String>, Ipv4Addr)> {
    let host_names = ingress
        .spec
        .as_ref()?
        .rules
        .as_ref()?
        .iter()
        .map(|d| d.host.as_ref())
        .filter(|d| d.is_some())
        .map(|d| {
            let mut d = d.unwrap().to_string();
            if !d.ends_with('.') {
                d.push('.');
            }
            d
        })
        .collect::<Vec<_>>();

    let ip = ingress
        .status
        .as_ref()?
        .load_balancer
        .as_ref()?
        .ingress
        .as_ref()?
        .first()?
        .ip
        .as_ref()?;

    Some((host_names, ip.parse().unwrap()))
}

async fn load_k8s_ingresses(
    local_entries: &mut RecordsMap,
) -> Result<(), Box<dyn std::error::Error>> {
    // Infer the runtime environment and try to create a Kubernetes Client
    let client = kube::Client::try_default().await?;

    // Extract IP addresses from ingresses resources
    let ingresses: kube::Api<Ingress> = kube::Api::all(client);
    for ingress in ingresses.list(&ListParams::default()).await? {
        if let Some((domains, ip)) = parse_ingress(&ingress) {
            domains.into_iter().for_each(|d| {
                log::info!("K8S ingress resolution: {} -> {}", d, ip);

                let name = Name::from_str(&d).expect("Failed to parse domain!");

                let mut record = Record::from_rdata(name.clone(), 60, RData::A(rdata::A::from(ip)));
                record.set_dns_class(DNSClass::IN);

                local_entries.insert((RecordType::A, name), record);
            })
        }
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    // Initialize crypto provider
    rustls::crypto::aws_lc_rs::default_provider()
        .install_default()
        .expect("Failed to install crypto provider!");

    let args = Args::parse();

    // Configure resolver
    let ns = args.resolver_config();

    let mut options = ResolverOpts::default();
    options.preserve_intermediates = true;

    let config = ResolverConfig::from_parts(None, vec![], ns);
    let resolver = TokioResolver::new(config, options, TokioConnectionProvider::default());

    let mut ip_transform = HashMap::new();
    for t in args.parse_transformations() {
        ip_transform.entry(t.src).or_insert(vec![]).push(t);
    }

    let mut local_entries = args.parse_local_records();

    // If requested, check K8s ingresses
    if args.k8s_ingress_support {
        if let Err(e) = load_k8s_ingresses(&mut local_entries).await {
            log::warn!("Failed to load K8S ingress entries! {e}");
        }
    }

    let config = Config {
        resolver,
        ip_transform,
        local_entries,
    };

    let authority = MyAuthority::new(Arc::new(config));
    let mut catalog = Catalog::new();
    catalog.upsert(LowerName::new(&Name::root()), vec![Arc::new(authority)]);

    let mut server = ServerFuture::new(catalog);

    // Start UDP servers
    let udp_socket = UdpSocket::bind(&args.listen_addr)
        .await
        .expect("Failed to listen on UDP socket!");
    server.register_socket(udp_socket);

    // Start TCP servers
    let tcp_socket = TcpListener::bind(&args.listen_addr)
        .await
        .expect("Failed to listen on TCP socket!");
    server.register_listener(tcp_socket, Duration::from_secs(5));

    log::info!("Starting to listen on {} ...", args.listen_addr);
    log::info!("Upstream server to use: {}", args.upstream_addr);
    log::info!("DNS over tls domain: {:?}", args.upstream_tls_name);
    match server.block_until_done().await {
        Ok(_) => {
            log::info!("DNS server is now stopping gracefully...");
        }
        Err(e) => {
            log::error!("Failed to initialize DNS server! {}", e)
        }
    }

    Ok(())
}
