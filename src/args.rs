use clap::Parser;
use hickory_resolver::config::NameServerConfigGroup;
use hickory_resolver::proto::rr::{rdata, DNSClass, RData, RecordType};
use hickory_resolver::Name;
use hickory_server::proto::rr::Record;
use std::collections::HashMap;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use std::str::FromStr;
use std::sync::Arc;

pub type RecordsMap = HashMap<(RecordType, Name), Record>;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IPTransformation {
    pub src: IpAddr,
    pub dst: IpAddr,
    pub net: ipnet::IpNet,
    pub exclude: Vec<ipnet::IpNet>,
}

/// Simple DNS transformer service
#[derive(Parser, Debug, Default)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    /// Listen address
    #[clap(short, long, default_value = "0.0.0.0:5300", env = "LISTEN_ADDR")]
    pub listen_addr: String,

    /// Upstream dns server to use, in the format IP:port
    #[clap(short, long, default_value = "1.1.1.1:53", env = "UPSTREAM_ADDR")]
    pub upstream_addr: String,

    /// Set this option to a non-empty value to enable DNS over TLS. This option must contain the domain of the certificate
    /// owned by the server.
    #[clap(short('s'), long, env = "UPSTREAM_TLS_NAME")]
    pub upstream_tls_name: Option<String>,

    /// IP transformations to apply
    ///
    ///
    ///
    /// In format: `SRC_IP&DEST_IP&TARGET_NETWORK` where:
    ///
    /// * SRC_IP is the ip to modify in the A / AAAA records
    ///
    /// * DEST_IP is the ip to set as replacement in the records
    ///
    /// * TARGET_NETWORK define the IP network where the modification shall take place. To apply
    ///   a modification for everyone, you could set `0.0.0.0/0` as network.
    ///
    ///
    /// Example: `5.5.5.9&192.168.1.5&10.0.0.0/8` will change all A records with value `5.5.5.9` into
    /// `192.168.1.5` for the DNS clients inside the network `10.0.0.0/8`
    ///
    ///
    /// It is also possible to exclude some subnetworks using this syntax
    /// `SRC_IP&DEST_IP&TARGET_NETWORK~EXCLUDE1~EXCLUDE2~...`
    ///
    /// You can repeat `~EXCLUDEN` as many time as you need it, for example:
    /// `5.5.5.9&192.168.1.5&10.0.0.0/8~10.0.3.0/24~10.0.1.1/32` will exclude the network `10.0.3.0`
    /// and the client with IP `10.0.1.1` from previous transformation.
    ///
    /// Note: multiple transformations can be put in the same argument, they must be separated by
    /// line breaks then
    ///
    /// Lines starting with '#' or ';' are also ignored
    #[clap(short, long, env = "TRANSFORMATIONS")]
    pub transformations: Vec<String>,

    /// Read Kubernetes ingress resources for IP transformation.
    ///
    /// If set to true, DNSProxy will connect to K8S api, get the list of defined ingress resources
    /// at startup, and reply with destination IP when a query is made on a domain hold by an ingress
    /// resource
    ///
    /// Note: IP transformations are apply AFTER Kubernetes Ingress resources transformation.
    #[clap(short, long, env = "INGRESS_SUPPORT")]
    pub k8s_ingress_support: bool,

    /// Custom dns records to be served in priority before fetching DNS server
    ///
    /// Specified entries must respect this format (on per line):
    ///
    /// DOMAIN RecordType Value
    ///
    /// Where:
    ///
    /// * DOMAIN is the domain associated with the record. It MUST be absolute, ie. finish with a dot
    ///
    /// * RecordType can be either 'A' 'AAAA' 'CNAME' 'NS' or 'MX' (other entries types are not yet supported)
    ///
    /// * Value is the specified value
    ///
    ///
    /// Value can be separated either by spaces or tabs. Multiple value can be defined in the same
    /// arguments, if each argument is separated by a new line caracter '\n'
    ///
    /// Lines starting with '#' or ';' are also ignored
    #[clap(short, long, env = "CUSTOM_RECORDS")]
    pub custom_records: Vec<String>,
}

impl Args {
    /// Get upstream DNS server address from CLI arguments
    pub fn resolver_config(&self) -> NameServerConfigGroup {
        let ip_split = self
            .upstream_addr
            .split_once(':')
            .expect("Upstream IP address must be in format: IP:port");

        let ip = ip_split.0.parse().expect("Invalid upstream IP address!");
        let port = ip_split.1.parse().expect("Invalid upstream DNS port!");

        let dns_server = self.upstream_tls_name.as_deref().unwrap_or("");
        match dns_server.is_empty() {
            true => NameServerConfigGroup::from_ips_clear(&[ip], port, true),

            // DNS over TLS has been requested by user
            false => {
                // Load root ca certificates
                let mut roots = rustls::RootCertStore::empty();
                for cert in
                    rustls_native_certs::load_native_certs().expect("could not load platform certs")
                {
                    roots
                        .add(cert)
                        .expect("Failed to add a native certificate to store!");
                }

                let tls_client_config = rustls::ClientConfig::builder()
                    .with_root_certificates(roots)
                    .with_no_client_auth();

                NameServerConfigGroup::from_ips_tls(&[ip], port, dns_server.to_string(), true)
                    .with_client_config(Arc::new(tls_client_config))
            }
        }
    }

    /// Parse the IP transformations to operate
    pub fn parse_transformations(&self) -> Vec<IPTransformation> {
        let mut dest = Vec::with_capacity(self.transformations.len());

        for t in &self.transformations {
            for l in t.split('\n') {
                if l.is_empty() || l.starts_with('#') || l.starts_with(';') {
                    continue;
                }

                let parts = l.split('&').collect::<Vec<&str>>();
                if parts.len() != 3 {
                    panic!("IP transformations must be composed of three components!");
                }

                let target_nets = parts[2].split('~').collect::<Vec<&str>>();

                log::debug!("Parse the transformation line {}", l);

                let tr = IPTransformation {
                    src: parts[0]
                        .parse()
                        .expect("[TR] Failed to parse a source IP address!"),
                    dst: parts[1]
                        .parse()
                        .expect("[TR] Failed to parse a dest IP address!"),
                    net: target_nets[0]
                        .parse()
                        .expect("[TR] Failed to parse target network!"),
                    exclude: target_nets
                        .iter()
                        .skip(1)
                        .map(|n| n.parse().expect("[TR] Failed to parse excluded network!"))
                        .collect(),
                };

                if tr.src.is_ipv4() != tr.dst.is_ipv4() {
                    panic!("A transformation must match IP version on src and dest!")
                }

                log::info!("Transform {} -> {} for network {}", tr.src, tr.dst, tr.net);
                dest.push(tr)
            }
        }

        dest
    }

    /// Get the list of local defined records
    pub fn parse_local_records(&self) -> RecordsMap {
        let mut res = HashMap::new();

        for recs in &self.custom_records {
            for l in recs.split('\n') {
                if l.is_empty() || l.starts_with('#') || l.starts_with(';') {
                    continue;
                }

                let (_, domain, rtype, value) =
                    lazy_regex::regex_captures!(r#"([a-zA-Z0-9.]+\.)[ \t]+([A-Z]+)[ \t]+(.*)"#, l)
                        .unwrap_or_else(|| {
                            panic!("Failed to extract information from record '{l}' !")
                        });

                log::info!(
                    "Process DNS entry (domain = {} / rtype = {} / value = {})",
                    domain,
                    rtype,
                    value
                );

                let name = Name::from_str(domain).expect("Could not parse DNS name!");

                let (rtype, data) = match rtype {
                    "A" => {
                        let ip: Ipv4Addr = value
                            .parse()
                            .unwrap_or_else(|_| panic!("Failed to parse IPv4 address '{value}' !"));

                        (RecordType::A, RData::A(rdata::A::from(ip)))
                    }

                    "AAAA" => {
                        let ip: Ipv6Addr = value.parse().expect("Failed to parse IPv6 address!");

                        (RecordType::AAAA, RData::AAAA(rdata::AAAA::from(ip)))
                    }

                    "CNAME" => {
                        let target = Name::from_str(value).expect("Failed to parse CNAME value!");

                        (RecordType::CNAME, RData::CNAME(rdata::CNAME(target)))
                    }

                    "NS" => {
                        let target = Name::from_str(value).expect("Failed to parse NS value!");

                        (RecordType::NS, RData::NS(rdata::NS(target)))
                    }

                    "MX" => {
                        let (pref, mx_name) = value.split_once(' ').expect("Invalid MX format!");
                        let pref = pref.parse().expect("Unable to parse pref!");
                        let mx_name = Name::from_str(mx_name).expect("Failed to parse name!");

                        (RecordType::MX, RData::MX(rdata::MX::new(pref, mx_name)))
                    }

                    &_ => {
                        panic!("Unsupported record type! {rtype}")
                    }
                };
                let mut record = Record::from_rdata(name.clone(), 60, data);
                record.set_dns_class(DNSClass::IN);

                res.insert((rtype, name), record);
            }
        }

        res
    }
}

#[cfg(test)]
mod test {
    use crate::args::{Args, IPTransformation};
    use hickory_resolver::proto::rr::{rdata, RecordType};
    use hickory_resolver::Name;
    use hickory_server::proto::rr::rdata::{a, aaaa, mx};
    use hickory_server::proto::rr::RData;
    use ipnet::Ipv4Net;
    use std::net::{IpAddr, Ipv4Addr};
    use std::str::FromStr;

    #[test]
    fn test_no_transform() {
        let mut args = Args::default();
        args.transformations = vec![];
        assert_eq!(args.parse_transformations().len(), 0);
    }

    #[test]
    fn test_one_transform() {
        let mut args = Args::default();
        args.transformations = vec!["127.0.0.1&192.168.1.1&10.2.3.4/24".to_string()];
        let res = args.parse_transformations();
        assert_eq!(res.len(), 1);

        assert_eq!(
            res[0],
            IPTransformation {
                src: IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)),
                dst: IpAddr::V4(Ipv4Addr::new(192, 168, 1, 1)),
                net: ipnet::IpNet::V4(Ipv4Net::from_str("10.2.3.4/24").unwrap()),
                exclude: vec![],
            }
        );
    }

    #[test]
    fn test_one_transform_with_exclude() {
        let mut args = Args::default();
        args.transformations = vec!["127.0.0.1&192.168.1.1&10.2.3.0/24~10.2.3.5/32".to_string()];
        let res = args.parse_transformations();
        assert_eq!(res.len(), 1);

        assert_eq!(
            res[0],
            IPTransformation {
                src: IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)),
                dst: IpAddr::V4(Ipv4Addr::new(192, 168, 1, 1)),
                net: ipnet::IpNet::V4(Ipv4Net::from_str("10.2.3.0/24").unwrap()),
                exclude: vec![ipnet::IpNet::V4(Ipv4Net::from_str("10.2.3.5/32").unwrap())],
            }
        );
    }

    #[test]
    fn test_multiple_transform() {
        let mut args = Args::default();
        args.transformations = vec![
            "127.0.0.1&192.168.1.1&10.2.3.4/24\n127.0.0.2&192.168.1.2&10.2.3.4/24".to_string(),
            "127.0.0.3&192.168.1.3&10.2.3.5/24".to_string(),
        ];
        let res = args.parse_transformations();
        assert_eq!(res.len(), 3);

        assert_eq!(
            res[0],
            IPTransformation {
                src: IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)),
                dst: IpAddr::V4(Ipv4Addr::new(192, 168, 1, 1)),
                net: ipnet::IpNet::V4(Ipv4Net::from_str("10.2.3.4/24").unwrap()),
                exclude: vec![],
            }
        );

        assert_eq!(
            res[1],
            IPTransformation {
                src: IpAddr::V4(Ipv4Addr::new(127, 0, 0, 2)),
                dst: IpAddr::V4(Ipv4Addr::new(192, 168, 1, 2)),
                net: ipnet::IpNet::V4(Ipv4Net::from_str("10.2.3.4/24").unwrap()),
                exclude: vec![],
            }
        );

        assert_eq!(
            res[2],
            IPTransformation {
                src: IpAddr::V4(Ipv4Addr::new(127, 0, 0, 3)),
                dst: IpAddr::V4(Ipv4Addr::new(192, 168, 1, 3)),
                net: ipnet::IpNet::V4(Ipv4Net::from_str("10.2.3.5/24").unwrap()),
                exclude: vec![],
            }
        );
    }

    #[test]
    fn test_one_record() {
        let mut args = Args::default();
        args.custom_records = vec!["test.com. A 1.1.1.1".to_string()];
        let res = args.parse_local_records();
        assert_eq!(res.len(), 1);

        let name = Name::from_str("test.com.").unwrap();
        let entry = res
            .get(&(RecordType::A, name.clone()))
            .expect("Missing entry!");
        assert_eq!(entry.name(), &name);
        assert_eq!(entry.data(), Some(&RData::A(a::A::new(1, 1, 1, 1))))
    }

    #[test]
    fn test_multiple_records() {
        let mut args = Args::default();
        args.custom_records = vec![
            "test.com. A 1.1.1.1\ntest.com. AAAA ::1\nmail.com. MX 10 srv.mail.".to_string(),
            "other.com. CNAME test.com.\nmonserveur.com.   \t NS   mondns.com.".to_string(),
        ];
        let res = args.parse_local_records();
        assert_eq!(res.len(), 5);

        let name = Name::from_str("test.com.").unwrap();
        let entry = res
            .get(&(RecordType::A, name.clone()))
            .expect("Missing entry!");
        assert_eq!(entry.name(), &name);
        assert_eq!(entry.data(), Some(&RData::A(a::A::new(1, 1, 1, 1))));

        let name = Name::from_str("test.com.").unwrap();
        let entry = res
            .get(&(RecordType::AAAA, name.clone()))
            .expect("Missing entry!");
        assert_eq!(entry.name(), &name);
        assert_eq!(
            entry.data(),
            Some(&RData::AAAA(aaaa::AAAA::new(0, 0, 0, 0, 0, 0, 0, 1)))
        );

        let name = Name::from_str("mail.com.").unwrap();
        let entry = res
            .get(&(RecordType::MX, name.clone()))
            .expect("Missing entry!");
        assert_eq!(entry.name(), &name);
        assert_eq!(
            entry.data(),
            Some(&RData::MX(mx::MX::new(
                10,
                Name::from_str("srv.mail.").unwrap()
            )))
        );

        let name = Name::from_str("other.com.").unwrap();
        let entry = res
            .get(&(RecordType::CNAME, name.clone()))
            .expect("Missing entry!");
        assert_eq!(entry.name(), &name);
        assert_eq!(
            entry.data(),
            Some(&RData::CNAME(rdata::CNAME(
                Name::from_str("test.com.").unwrap()
            )))
        );

        let name = Name::from_str("monserveur.com.").unwrap();
        let entry = res
            .get(&(RecordType::NS, name.clone()))
            .expect("Missing entry!");
        assert_eq!(entry.name(), &name);
        assert_eq!(
            entry.data(),
            Some(&RData::NS(rdata::NS(
                Name::from_str("mondns.com.").unwrap()
            )))
        );
    }
}
