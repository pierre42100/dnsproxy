# DNSProxy
This projects aims to solve issues due to NAT & DNS.

Let's say you have a server on your home network.

* Your public IP address is `1.1.1.2`
* Your server private IP address is `192.168.1.2`
* You have a domain, `example.org` that points to `1.1.1.2`
* You have a NAT rule on your router to forward traffic from ports `80` & `443` to `192.168.1.2`

using your domain from your network ?This is great, because you can access your server from the internet. But now, what if you want to access your server


You then have three options :
* Configure an entry like `192.168.1.2 example.org` on the `/etc/hosts` file of the computers of your network
* If your router has this option, configure a custom DNS entry in its control panel
* Setup this service to modify (transform) some DNS responses. The responses can also be modified only for certain clients, based on their IP and their IP networks.

## Additional feature: K8S ingresses rules reading
This tool can also read the ingresses rules of a Kubernetes cluster to respond to DNS requests pointing on ingresses rules without needing to relay the request to upstream server.

Use the `-k` switch to achieve that.

> Warning! The ingresses rules are only read on startup. You will need to restart DNSProxy if your ingresses rules changes to make the change being taken in account.

## Additional feature: local records
You can also define DNS records that should be used in priority of upstream DNS server. Currently, only `A`, `AAAA`, `CNAME`, `NS` and `MX` records are supported.

Use the `-c` flag to achieve that. You can use this flag multiple time, and define multiple records within the same argument using line break.

Example of dns entries:

```
test.fr. CNAME up.fr.
up.fr. A 1.2.3.4
up.fr. AAAA ::55:5
```


## Requirements
* Compile-time : Rust
* Runtime : none


## Installation
You can either build the source by yourself, using:

```bash
cargo build --release
```

... or you can use our Docker image:

```
docker pull pierre42100/dns_proxy
```
